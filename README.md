# schemepy
LISP(Micro Scheme) interpreter written in Python 3, based on [(An ((Even Better) Lisp) Interpreter (in Python))](http://norvig.com/lispy2.html).
