from setuptools import setup, find_packages
import schemepy

setup(name='schemepy',
      version='1.0',
      description='Micro Scheme interpreter',
      author='adikoh',
      packages=find_packages(),
      entry_points = {
          'console_scripts': [
              'schemepy = schemepy.schemepy:main',
                          ],
      },
)

