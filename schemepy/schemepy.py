import os
import sys
import re

class Symbol(str): pass

def Sym(s, symbol_table={}):
    if s not in symbol_table: symbol_table[s] = Symbol(s)
    return symbol_table[s]

_quote, _if, _set, _define, _lambda, _begin, _definemacro, = map(Sym, 
"quote   if   set!  define   lambda   begin   define-macro".split())
 
_quasiquote, _unquote, _unquotesplicing = map(Sym,
"quasiquote   unquote   unquote-splicing".split())



class InPort(object):

    tokenizer = r'''\s*(,@|[('`,)]|"(?:[\\].|[^\\"])*"|;.*|[^\s('"`,;)]*)(.*)'''
    def __init__(self, file):
        self.file = file; self.line = ''
    def next_token(self):
        while True:
            if self.line == '': self.line = self.file.readline()
            if self.line == '': return eof_object
            token, self.line = re.match(InPort.tokenizer, self.line).groups()
            if token != '' and not token.startswith(';'):
                return token

class Env(dict):
    def __init__(self, parms=(),args=(), outer=None):
        self.update(zip(parms,args))
        self.outer = outer

    def find(self,var):
        return self if var in self else self.outer.find(var)


def add_globals(env):
    import math, operator as op
    env.update(vars(math))
    env.update(
     {'+':op.add,
      '-':op.sub,
      '*':op.mul,
      '/':op.truediv,
      'not':op.not_,
      '>':op.gt,
      '<':op.lt,
      '>=':op.ge,
      '<=':op.le,
      '=':op.eq, 
      'equal?':op.eq,
      'eq?':op.is_,
      'length':len,
      'cons':lambda x,y:[x]+y,
      'car':lambda x:x[0],
      'cdr':lambda x:x[1:],
      'append':op.add,  
      'list':lambda *x:list(x),
      'list?': lambda x:isa(x,list), 
      'null?':lambda x:x==[],
      'symbol?':lambda x: isa(x, Symbol),
      'exit':sys.exit,
      'print':print,
  })
    return env

global_env = add_globals(Env())

isa = isinstance

def eval(x, env=global_env):

    if isa(x, Symbol):             
        return env.find(x)[x]
    elif not isa(x, list):         
        return x                
    elif x[0] == 'quote':         
        (_, exp) = x
        return exp
    elif x[0] == 'if':          
        (_, test, conseq, alt) = x
        return eval((conseq if eval(test, env) else alt), env)
    elif x[0] == 'set!':           
        (_, var, exp) = x
        env.find(var)[var] = eval(exp, env)
    elif x[0] == 'define':
        (_, var, exp) = x
        if isa(var, list):
            exp = ['lambda', var[1:], exp]
            var  = var[0]
        env[var] = eval(exp, env)
    elif x[0] == 'lambda':         
        (_, vars, exp) = x
        return lambda *args: eval(exp, Env(vars, args, env))
    elif x[0] == 'begin':          
        for exp in x[1:]:
            val = eval(exp, env)
        return val
    else:                          
        exps = [eval(exp, env) for exp in x]
        proc = exps.pop(0)
        return proc(*exps)


eof_object = Symbol('#<eof-object>') 


def readchar(inport):

    if inport.line != '':
        ch, inport.line = inport.line[0], inport.line[1:]
        return ch
    else:
        return inport.file.read(1) or eof_object
 
def read(inport):

    def read_ahead(token):
        if '(' == token: 
            L = []
            while True:
                token = inport.next_token()
                if token == ')': return L
                else: L.append(read_ahead(token))
        elif ')' == token: raise SyntaxError('unexpected )')
        elif token in quotes: return [quotes[token], read(inport)]
        elif token is eof_object: raise SyntaxError('unexpected EOF in list')
        else: return atom(token)

    token1 = inport.next_token()
    return eof_object if token1 is eof_object else read_ahead(token1)


parse = read

quotes = {"'":_quote, "`":_quasiquote, ",":_unquote, ",@":_unquotesplicing}

def atom(token):
    
    if token == '#t': return True
    elif token == '#f': return False
    elif token[0] == '"': return token[1:-1]
    try: return int(token)
    except ValueError:
        try: return float(token)
        except ValueError:
            try: return complex(token.replace('i', 'j', 1))
            except ValueError:
                return Sym(token)

            
def to_string(x):
    if x is True: return "#t"
    elif x is False: return "#f"
    elif isa(x, Symbol): return x
    elif isa(x, str): return '"%s"' % x.encode('string_escape').replace('"',r'\"')
    elif isa(x, list): return '('+' '.join(map(to_string, x))+')'
    elif isa(x, complex): return str(x).replace('j', 'i')
    else: return str(x)
    
def load(filename):
    
    repl(None, InPort(open(filename)), None)
 
def repl(prompt='schemepy> ', inport=InPort(sys.stdin), out=sys.stdout):

    sys.stderr.write("schemepy used Python 3\n")
        
    while True:
        try:
            if prompt:
                sys.stderr.write(prompt)
                sys.stderr.flush()
            x = parse(inport)
            if x is eof_object: return
            val = eval(x)
            if val is not None and out:
                print(to_string(val) ,file=out,flush=False)
        except Exception as e:
            print('%s: %s' % (type(e).__name__, e))


def main():
    argv= sys.argv
    args = len(argv)

    if args is 2:
        load(argv[1])
    else:
        repl()
